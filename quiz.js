const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});

let countHalf = 0;
let countClue = 0;
let calledTimes = 0;
let win = 0;
const description = "Below you can see four answers, but please remember that only one of them is correct.";
const fiftyFifty = "\nIf you are not sure the answer, you can write 'fifty' and computer will take away two random wrong answers, but please be aware that you can use this option only once.";
const notFifty = "\nPlease remember that you cannot use option 50:50.";
const clueDescription = "\nIt's a difficult question, so you can write a 'clue' and I will give you the hint, but please be aware that you can ask for it only once.";
const notClue = "\nYou cannot count on any clue this time.";
const exitDescription = "\nWrite 'exit' if you want to finish the quiz.\n";
const finalAnswer = "What is your final answer?\n";
const correctAnswer = "\nCorrect answer!\n";
const wrongAnswer = " is not the right answer. ";
const usedFifty = "You've already used the option fifty.";
const clue = "clue";
const exit = "exit";
const fifty = "fifty";

let step = {

    easyLevel: {
        first: {
            question: "What's the name of the famous duo played by Jim Carrey and Jeff Daniels?",
            rightAnswerAsReg: /\bDumb and Dumber|c\b/,
            rightAnswer: "Dumb and Dumber",
            answers: {
                a: "\na) Liar Liar",
                b: "\nb) Fun with Dick and Jane",
                c: "\nc) Dumb and Dumber",
                d: "\nd) Because of Winn-Dixie\n",
            }
        },
        second: {
            question: "Which movie is the first one that won Oscars for Best Film and Best International Feature?",
            rightAnswerAsReg: /\bParasite|c\b/,
            rightAnswer: "Parasite",
            answers: {
                a: "\na) Amelie",
                b: "\nb) Danton",
                c: "\nc) Parasite",
                d: "\nd) Fanny and Alexander\n",
            }
        },
        third: {
            question: 'Which Polish director made a series of films known as "Dekalog"',
            rightAnswerAsReg: /\bKrzysztof Kieslowski|d\b/,
            rightAnswer: "Krzysztof Kieslowski",
            answers: {
                a: "\na) Andrzej Wajda",
                b: "\nb) Jerzy Kawalerowicz",
                c: "\nc) Wojciech Jerzy Has",
                d: "\nd) Krzysztof Kieslowski\n",
            }
        },
    },

    mediumLevel: {
        first: {
            question: "Who was the director of 'The Deer Hunter'?",
            rightAnswerAsReg: /\bMichael Cimino|a\b/,
            rightAnswer: "Michael Cimino",
            answers: {
                a: "\na) Michael Cimino",
                b: "\nb) Francis Ford Coppola",
                c: "\nc) Martin Scorsese",
                d: "\nd) Patryk Vega\n",
            }
        },
        second: {
            question: "Which movie made by Sergio Leone is considered as plagiarism of Akira Kurosawa's 'Yojimbo'?",
            rightAnswerAsReg: /\bA Fistful of Dollars|a\b/,
            rightAnswer: "A Fistful of Dollars",
            answers: {
                a: "\na) A Fistful of Dollars",
                b: "\nb) For a Few Dollars More",
                c: "\nc) The Good, the Bad and the Ugly",
                d: "\nd) The Great Silence\n",
            }
        },
        third: {
            question: "Which movie doesn't allude to the biography of the serial killer Ed Gein?",
            rightAnswerAsReg: /\bZodiac|c\b/,
            rightAnswer: "Zodiac",
            answers: {
                a: "\na) The Silence of the Lambs",
                b: "\nb) Psycho",
                c: "\nc) Zodiac",
                d: "\nd) The Texas Chain Saw Massacre\n",
            }
        },
    },

    hardLevel: {
        first: {
            question: 'Which part from the Old Testament of the Christian Bible is mentioned by Max Cady in the movie "Cape Fear" directed by Martin Scorsese?',
            rightAnswerAsReg: /\bThe Book of Job|b\b/,
            rightAnswer: "The Book of Job",
            clue: "\nHere's the hint. This part of the Bible includes conversations between God and Satan.\n",
            answers: {
                a: "\na) The Gospel According to Matthew",
                b: "\nb) The Book of Job",
                c: "\nc) The Book of Genesis",
                d: "\nd) The Song of Songs\n",
            }
        },
        second: {
            question: "Which director has shot on all seven continents?",
            rightAnswerAsReg: /\bWerner Herzog|d\b/,
            rightAnswer: "Werner Herzog",
            clue: "\nHere's the hint. This director also made very famous movies with Klaus Kinski.\n",
            answers: {
                a: "\na) George Lucas",
                b: "\nb) Bernardo Bertolucci",
                c: "\nc) Peter Jackson",
                d: "\nd) Werner Herzog\n",
            }
        },
        third: {
            question: 'Which director made the famous "The Apu Trilogy"',
            rightAnswerAsReg: /\bSatyajit Ray|c\b/,
            rightAnswer: "Satyajit Ray",
            clue: "\nHere's the hint. This director didn't make westerns or any movie about vampire or samurai.\n",
            answers: {
                a: "\na) Werner Herzog",
                b: "\nb) Sergio Leone",
                c: "\nc) Satyajit Ray",
                d: "\nd) Akira Kurosawa\n",
            }
        }
    }
};

let nextQuest = {
    question: "Which term is used to describe stylish Hollywood crime dramas from 40's and 50's?",
    rightAnswerAsReg: /\bFilm noir|c\b/,
    rightAnswer: "Film noir",
    answers: {
        a: "\na) Neo-noir",
        b: "\nb) Swashbuckler films",
        c: "\nc) Film noir",
        d: "\nd) Peplum\n",
    }
};

step.easyLevel.fourth = nextQuest;

function selectQuest() {
    calledTimes++;
    if (calledTimes === 1) {
        console.log("Let's start with a very easy question.");
        let long = Object.keys(step.easyLevel);
        return long[Math.floor(Math.random() * long.length)];
    } else if (calledTimes === 2) {
        let long = Object.keys(step.mediumLevel);
        return long[Math.floor(Math.random() * long.length)];
    } else if (calledTimes > 2) {
        let long = Object.keys(step.hardLevel);
        return long[Math.floor(Math.random() * long.length)];
    }
}

function fiftyResult(material) {
    let passed = [];
    let obiekt = Object.values(material.answers);
    for (let element = 0; element < obiekt.length; element++) {
        if (obiekt[element].includes(material.rightAnswer)) {
            passed = obiekt[element];
            break;
        }
    }
    let next = obiekt[Math.floor(Math.random() * obiekt.length)];
    while (next === passed) {
        next = obiekt[Math.floor(Math.random() * obiekt.length)];
    }
    if (passed < next) {
        console.log(passed + next);
    } else {
        console.log(next + passed);
    }
    countHalf++;
}

function one() {
    console.log(description + fiftyFifty + exitDescription);
    const selected = selectQuest();
    const firstStep = step.easyLevel[selected];

    rl.question(firstStep.question + Object.values(firstStep.answers), (firstAnswer) => {
        if (firstStep.rightAnswerAsReg.test(firstAnswer) === true) {
            console.log(correctAnswer);
            delete step.easyLevel[selected];
            two();
        } else if (firstAnswer.trim() === exit) {
            rl.close();
        } else if (firstAnswer.trim() === fifty) {
            fiftyResult(firstStep)
            rl.question(finalAnswer, (firstAnswer) => {
                if (firstStep.rightAnswerAsReg.test(firstAnswer) === true) {
                    console.log(correctAnswer);
                    delete step.easyLevel[selected];
                    two()
                } else {
                    rl.setPrompt(firstAnswer + wrongAnswer);
                    rl.prompt();
                    rl.close();
                }
            })
        } else {
            rl.setPrompt(firstAnswer + wrongAnswer);
            rl.prompt();
            rl.close();
        }
    })
}
one();

function two() {
    if (countHalf === 0) {
        console.log(description + fiftyFifty + exitDescription);
    } else if (countHalf > 0) {
        console.log(description + notFifty + exitDescription);
    }
    const selected2 = selectQuest();
    const secondStep = step.mediumLevel[selected2];

    rl.question(secondStep.question + Object.values(secondStep.answers), (secondAnswer) => {
        // //If the rl.setPrompt or rl.question contains \n,
        //the whole sentence will be repeated every time when
        //you use backspace. Don't know how to fix it.
        if (secondStep.rightAnswerAsReg.test(secondAnswer) === true) {
            console.log(correctAnswer);
            delete step.mediumLevel[selected2];
            three();
        } else if (secondAnswer.trim() === exit) {
            rl.close();
        } else if (secondAnswer.trim() === fifty) {
            if (countHalf === 0) {
                fiftyResult(secondStep)
                rl.question(finalAnswer, (secondAnswer) => {
                    if (secondStep.rightAnswerAsReg.test(secondAnswer) === true) {
                        console.log(correctAnswer);
                        delete step.mediumLevel[selected2];
                        three();
                    } else {
                        rl.setPrompt(secondAnswer + wrongAnswer);
                        rl.prompt();
                        rl.close();
                    }
                })
            } else if (countHalf > 0) {
                countHalf++;
                console.log(usedFifty);
                rl.question(finalAnswer, (secondAnswer) => {
                    if (secondStep.rightAnswerAsReg.test(secondAnswer) === true) {
                        console.log(correctAnswer);
                        delete step.mediumLevel[selected2];
                        three();
                    } else {
                        rl.setPrompt(secondAnswer + wrongAnswer);
                        rl.prompt();
                        rl.close();
                    }
                })
            }
        } else {
            rl.setPrompt(secondAnswer + wrongAnswer);
            rl.prompt();
            rl.close();
        }
    })
}

function three() {
    if (countHalf === 0) {
        console.log(description + fiftyFifty + clueDescription + exitDescription);
    } else if (countHalf > 0) {
        console.log(description + notFifty + clueDescription + exitDescription);
    }
    const selected3 = selectQuest();
    const thirdStep = step.hardLevel[selected3];

    rl.question(thirdStep.question + Object.values(thirdStep.answers), (thirdAnswer) => {
        if (thirdStep.rightAnswerAsReg.test(thirdAnswer) === true) {
            console.log(correctAnswer);
            delete step.hardLevel[selected3];
            four();
        } else if (thirdAnswer.trim() === exit) {
            rl.close();
        } else if (thirdAnswer.trim() === fifty) {
            if (countHalf === 0) {
                fiftyResult(thirdStep)
                console.log(clueDescription);
                rl.question(finalAnswer, (thirdAnswer) => {
                    if (thirdStep.rightAnswerAsReg.test(thirdAnswer) === true) {
                        console.log(correctAnswer);
                        delete step.hardLevel[selected3];
                        four();
                    } else if (thirdAnswer.trim() === clue) {
                        countClue++;
                        rl.question(thirdStep.clue, (thirdAnswer) => {
                            if (thirdStep.rightAnswerAsReg.test(thirdAnswer) === true) {
                                console.log(correctAnswer);
                                delete step.hardLevel[selected3];
                                four();
                            } else {
                                rl.setPrompt(thirdAnswer + wrongAnswer);
                                rl.prompt();
                                rl.close();
                            }
                        })
                    } else {
                        rl.setPrompt(thirdAnswer + wrongAnswer);
                        rl.prompt();
                        rl.close();
                    }
                })
            } else {
                countHalf++; // todo It's better to leave it, 
                // because I can decide in the future to increase 
                // the number of times that I allow the player to use
                // this option
                console.log(usedFifty + clueDescription);
                rl.question(finalAnswer, (thirdAnswer) => {
                    if (thirdStep.rightAnswerAsReg.test(thirdAnswer) === true) {
                        console.log(correctAnswer);
                        delete step.hardLevel[selected3];
                        four();
                    } else if (thirdAnswer.trim() === clue) {
                        countClue++;
                        rl.question(thirdStep.clue, (thirdAnswer) => {
                            if (thirdStep.rightAnswerAsReg.test(thirdAnswer) === true) {
                                console.log(correctAnswer);
                                delete step.hardLevel[selected3];
                                four();
                            } else {
                                rl.setPrompt(thirdAnswer + wrongAnswer);
                                rl.prompt();
                                rl.close();
                            }
                        })
                    } else {
                        rl.setPrompt(thirdAnswer + wrongAnswer);
                        rl.prompt();
                        rl.close();
                    }
                })
            }
        } else if (thirdAnswer.trim() === clue) {
            if (countHalf > 0) {
                countClue++;
                console.log(thirdStep.clue + notFifty);
                rl.question(finalAnswer, (thirdAnswer) => {
                    if (thirdStep.rightAnswerAsReg.test(thirdAnswer) === true) {
                        console.log(correctAnswer);
                        delete step.hardLevel[selected3];
                        four();
                    } else {
                        rl.setPrompt(thirdAnswer + wrongAnswer);
                        rl.prompt();
                        rl.close();
                    }
                })
            } else if (countHalf === 0) {
                countClue++;
                console.log(thirdStep.clue + fiftyFifty);
                rl.question(finalAnswer, (thirdAnswer) => {
                    if (thirdStep.rightAnswerAsReg.test(thirdAnswer) === true) {
                        console.log(correctAnswer);
                        delete step.hardLevel[selected3];
                        four();
                    } else if (thirdAnswer.trim() === fifty) {
                        fiftyResult(thirdStep)
                        rl.question(finalAnswer, (thirdAnswer) => {
                            if (thirdStep.rightAnswerAsReg.test(thirdAnswer) === true) {
                                console.log(correctAnswer);
                                delete step.hardLevel[selected3];
                                four();
                            } else {
                                rl.setPrompt(thirdAnswer + wrongAnswer);
                                rl.prompt();
                                rl.close();
                            }
                        })
                    } else {
                        rl.setPrompt(thirdAnswer + wrongAnswer);
                        rl.prompt();
                        rl.close();
                    }
                })
            }
        } else {
            rl.setPrompt(thirdAnswer + wrongAnswer);
            rl.prompt();
            rl.close();
        }
    })
}

function four() {
    if (countHalf === 0 && countClue === 0) {
        console.log(description + fiftyFifty + clueDescription + exitDescription);
    } else if (countHalf === 0 && countClue > 0) {
        console.log(description + fiftyFifty + notClue + exitDescription);
    } else if (countHalf > 0 && countClue === 0) {
        console.log(description + notFifty + clueDescription + exitDescription);
    } else if (countHalf > 0 && countClue > 0) {
        console.log(description + notFifty + notClue + exitDescription);
    }
    const selected4 = selectQuest();
    const fourthStep = step.hardLevel[selected4];

    rl.question(fourthStep.question + Object.values(fourthStep.answers), (fourthAnswer) => {
        if (fourthStep.rightAnswerAsReg.test(fourthAnswer) === true) {
            console.log(correctAnswer);
            delete step.hardLevel[selected4];
            win++;
            rl.close();
        } else if (fourthAnswer.trim() === exit) {
            rl.close();
        } else if (fourthAnswer.trim() === fifty) {
            if (countHalf === 0) {
                fiftyResult(fourthStep)
                if (countClue === 0) {
                    console.log(clueDescription);
                    rl.question(finalAnswer, (fourthAnswer) => {
                        if (fourthStep.rightAnswerAsReg.test(fourthAnswer) === true) {
                            console.log(correctAnswer);
                            delete step.hardLevel[selected4];
                            win++;
                            rl.close();
                        } else if (fourthAnswer.trim() === clue) {
                            countClue++;
                            rl.question(fourthStep.clue, (fourthAnswer) => {
                                if (fourthStep.rightAnswerAsReg.test(fourthAnswer) === true) {
                                    console.log(correctAnswer);
                                    delete step.hardLevel[selected4];
                                    win++;
                                    rl.close();
                                } else {
                                    rl.setPrompt(fourthAnswer + wrongAnswer);
                                    rl.prompt();
                                    rl.close();
                                }
                            })
                        } else {
                            rl.setPrompt(fourthAnswer + wrongAnswer);
                            rl.prompt();
                            rl.close();
                        }

                    })
                } else if (countClue > 0) {
                    console.log(notClue);
                    rl.question(finalAnswer, (fourthAnswer) => {
                        if (fourthStep.rightAnswerAsReg.test(fourthAnswer) === true) {
                            console.log(correctAnswer);
                            delete step.hardLevel[selected4];
                            win++;
                            rl.close();
                        } else {
                            rl.setPrompt(fourthAnswer + wrongAnswer);
                            rl.prompt();
                            rl.close();
                        }

                    })
                }
            } else if (countHalf > 0) {
                if (countClue === 0) {
                    console.log(usedFifty + clueDescription);
                    rl.question(finalAnswer, (fourthAnswer) => {
                        if (fourthStep.rightAnswerAsReg.test(fourthAnswer) === true) {
                            console.log(correctAnswer);
                            delete step.hardLevel[selected4];
                            win++;
                            rl.close();
                        } else if (fourthAnswer.trim() === clue) {
                            countClue++;
                            rl.question(fourthStep.clue, (fourthAnswer) => {
                                if (fourthStep.rightAnswerAsReg.test(fourthAnswer) === true) {
                                    console.log(correctAnswer);
                                    delete step.hardLevel[selected4];
                                    win++;
                                    rl.close();
                                } else {
                                    rl.setPrompt(fourthAnswer + wrongAnswer);
                                    rl.prompt();
                                    rl.close();
                                }
                            })
                        } else {
                            rl.setPrompt(fourthAnswer + wrongAnswer);
                            rl.prompt();
                            rl.close();
                        }
                    })
                } else if (countClue > 0) {
                    console.log(usedFifty + notClue);
                    rl.question(finalAnswer, (fourthAnswer) => {
                        if (fourthStep.rightAnswerAsReg.test(fourthAnswer) === true) {
                            console.log(correctAnswer);
                            delete step.hardLevel[selected4];
                            win++;
                            rl.close();
                        } else {
                            rl.setPrompt(fourthAnswer + wrongAnswer);
                            rl.prompt();
                            rl.close();
                        }
                    })
                }
            }
        } else if (fourthAnswer.trim() === clue) {
            if (countHalf === 0 && countClue === 0) {
                console.log(fourthStep.clue + fiftyFifty);
                countClue++;

                rl.question(finalAnswer, (fourthAnswer) => {
                    if (fourthStep.rightAnswerAsReg.test(fourthAnswer) === true) {
                        console.log(correctAnswer);
                        delete step.hardLevel[selected4];
                        win++;
                        rl.close();
                    } else if (fourthAnswer.trim() === fifty) {
                        fiftyResult(fourthStep)
                        rl.question(finalAnswer, (fourthAnswer) => {
                            if (fourthStep.rightAnswerAsReg.test(fourthAnswer) === true) {
                                console.log(correctAnswer);
                                delete step.hardLevel[selected4];
                                win++;
                                rl.close();
                            } else {
                                rl.setPrompt(fourthAnswer + wrongAnswer);
                                rl.prompt();
                                rl.close();
                            }
                        })
                    } else {
                        rl.setPrompt(fourthAnswer + wrongAnswer);
                        rl.prompt();
                        rl.close();
                    }
                })
            } else if (countHalf === 0 && countClue > 0) {
                console.log(notClue + fiftyFifty);

                rl.question(finalAnswer, (fourthAnswer) => {
                    if (fourthStep.rightAnswerAsReg.test(fourthAnswer) === true) {
                        console.log(correctAnswer);
                        delete step.hardLevel[selected4];
                        win++;
                        rl.close();
                    } else if (fourthAnswer.trim() === fifty) {
                        fiftyResult(fourthStep)
                        rl.question(finalAnswer, (fourthAnswer) => {
                            if (fourthStep.rightAnswerAsReg.test(fourthAnswer) === true) {
                                console.log(correctAnswer);
                                delete step.hardLevel[selected4];
                                win++;
                                rl.close();
                            } else {
                                rl.setPrompt(fourthAnswer + wrongAnswer);
                                rl.prompt();
                                rl.close();
                            }
                        })
                    } else {
                        rl.setPrompt(fourthAnswer + wrongAnswer);
                        rl.prompt();
                        rl.close();
                    }
                })
            } else if (countHalf > 0 && countClue === 0) {
                console.log(fourthStep.clue + notFifty);
                countClue++;

                rl.question(finalAnswer, (fourthAnswer) => {
                    if (fourthStep.rightAnswerAsReg.test(fourthAnswer) === true) {
                        console.log(correctAnswer);
                        delete step.hardLevel[selected4];
                        win++;
                        rl.close();
                    } else {
                        rl.setPrompt(fourthAnswer + wrongAnswer);
                        rl.prompt();
                        rl.close();
                    }
                })
            } else if (countHalf > 0 && countClue > 0) {
                console.log(notClue + notFifty);

                rl.question(finalAnswer, (fourthAnswer) => {
                    if (fourthStep.rightAnswerAsReg.test(fourthAnswer) === true) {
                        console.log(correctAnswer);
                        delete step.hardLevel[selected4];
                        win++;
                        rl.close();
                    } else {
                        rl.setPrompt(fourthAnswer + wrongAnswer);
                        rl.prompt();
                        rl.close();
                    }
                })
            }
        } else {
            rl.setPrompt(fourthAnswer + wrongAnswer);
            rl.prompt();
            rl.close();
        }
    })
}

rl.on('close', () => {
    // console.log(step) todo You can check by yourself if the questions were erased.
    if (calledTimes < 2) {
        console.log("You didn't answer any question.");
    } else if (calledTimes === 2) {
        console.log("You answered the first question.");
    } else if (calledTimes === 3) {
        console.log("You answered two questions.");
    } else if (calledTimes === 4 && win === 0) {
        console.log("You answered three questions.");
    } else if (calledTimes === 4 && win === 1) {
        console.log("Congratulations. You answered all questions.");
    }
    process.exit(0);
})