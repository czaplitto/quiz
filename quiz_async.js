const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});

const question = (str) => new Promise(resolve => rl.question(str, resolve));

let step = {
    first: {
        question: "What's the name of the famous duo played by Jim Carrey and Jeff Daniels?",
        rightAnswer: "Dumb and Dumber",
        answers: {
            a: "\na) Liar Liar",
            b: "\nb) Fun with Dick and Jane",
            c: "\nc) Dumb and Dumber",
            d: "\nd) Because of Winn-Dixie\n",
        }
    },
    second: {
        question: "Which movie is the first one that won Oscars for Best Film and Best International Feature?",
        rightAnswer: "Parasite",
        answers: {
            a: "\na) Amelie",
            b: "\nb) Danton",
            c: "\nc) Parasite",
            d: "\nd) Fanny and Alexander\n",
        }
    },
    third: {
        question: 'Which Polish director made a series of films known as "Dekalog"',
        rightAnswer: "Krzysztof Kieslowski",
        answers: {
            a: "\na) Andrzej Wajda",
            b: "\nb) Jerzy Kawalerowicz",
            c: "\nc) Wojciech Jerzy Has",
            d: "\nd) Krzysztof Kieslowski\n",
        }
    }
}

let firstStep = step.first
let secondStep = step.second

const stage = {
    seeFirst: async () => {
        const seeFirst = await question(firstStep.question + Object.values(firstStep.answers))

        if (firstStep.rightAnswer === seeFirst) {
            console.log("\nPoprawna odpowiedź!\n")
            stage.seeSecond()
        } else {
            rl.setPrompt(`${seeFirst} is the wrong answer`);
            rl.prompt();
            stage.failed()
        }
    },
    seeSecond: async () => {
        const seeSecond = await question(secondStep.question + Object.values(secondStep.answers))

        if (secondStep.rightAnswer === seeSecond) {
            console.log("\nPoprawna odpowiedź!")
            stage.end()
        } else {
            rl.setPrompt(`${seeSecond} is the wrong answer`);
            rl.prompt();
            stage.failed()
        }
    },
    failed: async () => { // todo do I need to use async or write failed() {code}?
        console.log("\nYou can always try again.");
        // rl.close()
        process.exit(0)
    },
    end: async () => { // todo do I need to use async?
        console.log("Poprawnie rozwiązałeś test.");
        // rl.close()
        process.exit(0)
    },
}

stage.seeFirst()